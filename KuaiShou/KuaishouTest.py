import requests,re,random,json
from selenium import webdriver
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
import time
from PIL import Image


def getWebUserInfo():
    '''分享地址测试'''
    headers = {
        "Host": "v.kuaishou.com",
        "User-Agent": "Mozilla/5.0 (iPad; CPU OS 11_0 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) Version/11.0 Mobile/15A5341f Safari/604.1",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Accept-Language": "zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2",
        "Accept-Encoding": "gzip, deflate, br",
        "Connection": "keep-alive",
        "Upgrade-Insecure-Requests": "1"
    }
    print(headers)

    url = "https://v.kuaishou.com/p_vSE"
    print(url)
    oneRes = requests.get(url, headers=headers, timeout=20,allow_redirects=False)
    oneResHeaders = oneRes.headers
    cookiesString = oneResHeaders["Set-Cookie"]
    did = re.findall(r"did=web_(.*?);",cookiesString)[0]
    didv = re.findall(r"didv=(\d*?);", cookiesString)[0]
    cookies = "did=web_{}; didv={};".format(did,didv)
    print(cookies)

def getH5UserInfo(userId):
    '''自定义地址测试'''
    headers = {
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "zh-CN,zh;q=0.9,en;q=0.8,zh-TW;q=0.7,da;q=0.6,ja;q=0.5,und;q=0.4",
        "Cache-Control": "no-cache",
        "Connection": "keep-alive",
        "Host": "m.gifshow.com",
        "Pragma": "no-cache",
        "Cookie": "did=web_364d45126146cddf443adf{};".format(random.randint(1000000000, 9999999999)),
        "Upgrade-Insecure-Requests": "1",
        "User-Agent": "Mozilla/5.0 (iPad; CPU OS 11_0 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) Version/11.0 Mobile/15A5341f Safari/604.1"
    }

    url = "http://m.gifshow.com/fw/user/{}?fid={}&cc=share_copylink&appType=21&shareId=1065{}&shareType=3".format(userId, random.randint(10000000, 99999999), random.randint(10000000, 99999999))
    print(url)
    # headers["Cookie"] = "didv=1588123703000; did=web_25da43547ba7474180bc88435b3494bd;"
    res = requests.get(url, headers=headers, timeout=20)
    result = res.text
    print(result)

def isElementExist(browser,element):
    flag = True

    try:
        browser.find_element_by_css_selector(element)
        return flag

    except:
        flag = False
        return flag


def main_check_code(driver,element):
    return ""


def move_to(brower,element,x):

    # element = brower.find_element_by_xpath(s3)
    ActionChains(brower).click_and_hold(on_element=element).perform()
    ActionChains(brower).move_to_element_with_offset(to_element=element, xoffset=x, yoffset=0).perform()
    time.sleep(3)
    ActionChains(brower).release(on_element=element).perform()


def test():
    # 图片阀值
    Image.open('cap_union_new_getcapbysig.jpeg').convert('L').save('cap4.jpg')
    # im=Image.open("cap4.jpg")

    x = convert('cap_union_new_getcapbysig.jpeg', 'cap2.png')
    print("x=", x)


def convert(imgName, saveName):
    im = Image.open(imgName)
    im = im.convert('RGBA')

    width = im.size[0]
    height = im.size[1]
    listx = []
    for h in range(0, height):
        for w in range(0, width):
            pixel = im.getpixel((w, h))
            if (width - w > 2):
                pixel2 = im.getpixel((w + 2, h))
                sr = pixel[0] - pixel2[0]
                sg = pixel[1] - pixel2[1]
                sb = pixel[2] - pixel2[2]
                if sr > 110 and sg > 110 and sb > 110:
                    print("r", sr, "g", sg, "b", sb)
                    if len(listx) > 0:
                        listxitem = set(listx)
                        for item in listxitem:
                            if listx.count(item) > 8:
                                return item
                    listx.append(w)

            # if pixel[0] > 0 and pixel[1] > 0 and pixel[2] > 0 and pixel[3] > 0:
            #     im.putpixel((w, h), (0,0,0, 220))
            # elif pixel[0] == 0 and pixel[1] == 0 and pixel[2] == 0:
            #     im.putpixel((w, h), (0, 0, 0, 220))
            # else:
            #     pass
        # 连续8次 横坐标一样判断位滑动的位置

    im.save(saveName)

def TrackSlide(driver):
    try:
        count = 6  # 最多识别6次
        # 等待滑动按钮加载完成
        #切换frame
        tcaptcha_iframe=driver.switch_to.frame("tcaptcha_iframe")


        #获取滑动轴
        slide_bar_head = driver.find_element_by_id("slide_bar_head")
        #x 滑动的距离

        #获取识别的距离
        x=150
        move_to(driver,slide_bar_head,x)
        time.sleep(3)



    finally:
        driver.close()


def getShopList2():
    try:
         url="https://www.kwaishop.com/merchant/shop/list?id=330143373"
         driver = webdriver.Chrome("/Users/yangfan/Downloads/chromedriver")
         driver.get(url)
         driver.implicitly_wait(3)
         print(driver.title)
         # time.sleep(5)
         if(isElementExist(driver,"verify_btn")):
             print("验证码验证")
             #按钮
             verify_btn = driver.find_element_by_id("verify_btn")
             #文字输入框
             cap_input = driver.find_element_by_id("cap_input")
             #图片
             cap_que_img = driver.find_element_by_id("cap_que_img")
             img_url = cap_que_img.get_attribute('src')
             cap_input.send_keys("1234")
             verify_btn.click()

         else:
             # time.sleep(5)
             TrackSlide(driver)
             print("滑动验证")




    except Exception  as e:
        print("异常",e)
        # 关闭当前页面
        #  driver.close()

    driver.quit()



def getShopList():
    headers = {
        # "Accept": "application/json",
        # "Content-Type": "application/json; charset=UTF-8",
        "Referer":"https://www.kwaishop.com/merchant/shop/list?id=330143373",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36",
    }
    ua="TW96aWxsYS81LjAgKFdpbmRvd3MgTlQgMTAuMDsgV2luNjQ7IHg2NCkgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lLzc5LjAuMzk0NS43OSBTYWZhcmkvNTM3LjM2"
    url="https://www.kwaishop.com/rest/app/grocery/ks/shop/score?sellerId=330143373"
    result = requests.get(url,headers=headers, timeout=10)
    r=result.text
    print(r)
    j_str=json.loads(r)
    error_code=j_str["result"]



    if(error_code==9200):
        error_url=str(j_str["errorUrl"])
        # print(error_url)
        key=error_url[error_url.index("key=")+4:error_url.index("&type")]
        print('key=',key)
        url="https://id.kuaishou.com/rest/c/infra/captcha/get?key={}&type=4&uri=%2frest%2fapp%2fgrocery%2fks%2fshop%2fscore&provider=tencent&device=mobile".format(key)
        result = requests.post(url,data='{}',headers=headers,timeout=10)
        # print(result.text)
        r=result.text
        j_str = json.loads(r)
        error_url =str(j_str["data"])
        asig=error_url[error_url.index("asig=")+5:]
        print('asig=',asig)

        #获取sess  sid
        url="https://captcha.guard.qcloud.com/cap_union_prehandle?asig={}&aid=1251633352&captype=&protocol=https&clientype=1&disturblevel=&apptype=&noheader=&color=&showtype=&fb=1&theme=&lang=2052&ua={}&cap_cd=&uid=&callback=_aq_389654&sess=&subsid=1".format(asig,ua)
        result = requests.get(url, headers=headers, timeout=10)
        r = result.text
        # print(r)
        r=r[r.index("(")+1: len(r)-1]
        # print(r)
        j_str = json.loads(r)
        sess=j_str["sess"]
        print(sess)
        sid = j_str["sid"]
        print(sid)




        #获取websig  vsig
        url="https://captcha.guard.qcloud.com/cap_union_new_show?asig={}&aid=1251633352&captype=&protocol=https&clientype=1&disturblevel=&apptype=&noheader=&color=&showtype=&fb=1&theme=&lang=2052&ua={}&sess={}&fwidth=0&sid={}&subsid=2&uid=&cap_cd=&rnd=517498&forcestyle=undefined&wxLang=&TCapIframeLoadTime=537&prehandleLoadTime=126&createIframeStart=1591084534351".format(asig,ua,sess,sid)
        driver=webdriver.Chrome("/Users/yangfan/Downloads/chromedriver")
        driver.get(url)
        print(driver.title)
        verify_btn=driver.find_element_by_id("verify_btn")
        cap_input = driver.find_element_by_id("cap_input")
        cap_input.send_keys("1234")
        verify_btn.click()

        driver.quit()



        # result = requests.get(url, headers=headers, timeout=10)
        # r = str(result.text)
        # print(r)
        # print(str(r.index("&")))
        # websig=r[r.find("websig=")+7:r.find("&cdata")]
        # print("websig=",websig)


        #eks   acaecc跟M 有关


        #验证码识别
        url="https://captcha.guard.qcloud.com/cap_union_new_verify"
        postdata="asig={}&aid=1251633352&captype=&protocol=https&clientype=1&disturblevel=&apptype=&noheader=&color=&showtype=&fb=1&theme=&lang=2052&ua={}&sess={}&fwidth=0&sid={}&subsid=13&uid=&cap_cd=&rnd=247151&forcestyle=undefined&wxLang=&TCapIframeLoadTime=107&prehandleLoadTime=55&createIframeStart=1590746319828&rand=0.3089105372531147&buid=&subcapclass=9&vsig=b01k_4sblMGVSmeLu4wldzeFKJGrnMymQDisIZIg9EYkFCHjm9RzF7fH_1pP8vkGt6ISbxi8xLBuIW29xC9U47yPMd1y9Q3TNo_HlXGHhfPomyw4P3yO8-4zQ**&ans=276,22;&aaccaa=qkRozx%2FV1MLqRQRatLzMJwT7SaeQzYMBTFaVCJVp0up9Gv2ctRKAWkl33y%2Ftldxltqp2Jr%2BZjYSBP%2BAW7%2FcbwDbBT9UjycF%2B7OjAFRU0N7Suy5hV53ieUReGCabuzBvQq0dHtbKSPCe%2FwJd20PXCpR5%2FNsrtRDSoSUDYKdfwtsF%2BgjsrF83KvaATDhoEmjf7k%2Fqooruv750C85n1hgJZsVq14LIkcRAm1e32IcPXfviONnhlzXqV5Bc4e0OpOg6iaqoSeW93ITYRHnAKEbHlkpgMXpJhnpvRBXOSJOGg3YsIoBJcDHwT9gdpbwFP0SoqVsfhN86ZlPenn7q8gZlgBAGusuN3ORH7FEHWjEahgyONtJOCTPqBbkl33y%2FtldxlC%2FdZnFj%2FyiV40IT4yejsEXTPg4Yb3v9mxL3QaZPG4YbrcdNe7N4LJp6vCgWH65yk96F2oEgDVYU4NGotGXvQdp3UgFGfQlMdaw9h9fmUauHLvNZJ4SMUSHZPNklsvX0S2w12sfnT7ICtAaZamSv%2B8Lm%2ByGdBctoe2JDQWr5LZ7FRxi46yCrTWKzQcLX2m2Zo9INBp9IXlxwiGigMlEz7nLiX16%2BJVpjLSPgnw43%2FkUaTYtyOq6TrBc%2Bumom37RpIrCOdKUpb3BS0DRCAaDxDNNaXT80UOLudVka5M7CWw6gRcm7DhbSQH9YeoCgHPZTtguB3qHIcUORBphKuu%2FuoDAVHW4teFOrFhS13Z7ROE%2FAvFJ%2FjB8tqMxh25VAaMjg1WlPUDhk9fP1We%2FIQL2uRIl7SnMq%2FdxVRkKBDR5VxLFnnPuMBWHDCpa%2F9AXuBxbPExx%2F7FR7fB%2FX9WW3AHssiF%2BLvnukd97Vk9gL89UOozCEFwWyjsaX6oKEbftE5av2yrzobvODKVIyNhJQM97nf2PpinKPYKL7OJ80lJxCemSBTSIaf%2F1z1PabT201ku7FHp7EAYCtAAs5smydiNQjnWtVvwTKLYomYuBhanc6X%2FqEH5JuUmEXgxmFz5Wj3iJkszJzS68i1Ijp%2FpABlboLSfwDpK%2BjxGrUhuwtSaE5ZMnX5VHYBxvxhrbDzdyUr9JxEYqg93pP36kZRxi46yCrTWFAP3yIAkhtnPcntjG%2FS0%2FFd84OMvhcLYJYFyBqh7BkwvfO045R537vWfW0BeNs2P2RJxaPUYPqKh9BR7S13H8lCFgX0X35qHyH0GFPLBvFN9f6WBQ8AyqNwf9htRG7pygem3WffMUZkOv2dWc51T1FI%2BCfDjf%2BRRkJKixGD18%2BD90ff1RVcEo0h9BhTywbxTdzLaI3JcEE2oHKa%2FV8NecmOT2UNgky1WdkxyczlrmKNuQKODVq0sdvRZKQ3KZ%2F630JKixGD18%2BDxWSRfeo2GhOpEX4Xru9wXaD6L1MmeAT1xgWUkZc2RCUcJHqDAVC4dl9qRal3w1egUZqCSh1lsVzLecZb7MB%2FqNFkpDcpn%2FrfQkqLEYPXz4Ptrtsc3hDcR6kRfheu73BdvLpJ8r7sz9NKPVVuKm5Olcj3kj2%2BhqIJuQKODVq0sdvon5wvgPKdBGv5CIplwEPFODxq5ebLMcmr6LvfP3jJjurHg1CkvVA7X2pFqXfDV6DI95I9voaiCct5xlvswH%2Boh6gAzpiRz1jQBhVYyyWSrPZuNg%2FT%2FH4rYsZsyTroYLCkvLRn9x0ePe24HA82oPlzojDBxH3ARzlJd98v7ZXcZUl33y%2FtldxlRVK6VhoeoSRE5A%2FSPG3TM1BBVzKtQ5T7ysTZN8xbuC9nQat5nU7VwSIgrd%2Fi8aEE2a2kVxjmJceuD2EiHkN8QN%2FEj7vtM1UZUQ3%2BlBQHRGLIV3f3vmf3fVKUVLGiGhBjNCPKneY3aADeqtytaEnBF%2Fp6X2dQ1xu8kFETsWzCywG2p%2F%2BczPts9LQUfC%2B8P%2BZgadAFwIxvYBWk10YGq3S0T%2BUYNnz%2BXemOyh6YJonSFrNvrIBV0ushvUl33y%2Ftldxl7Xr066Ja9xjXdFafjOAfY9oy3No6YUdDlGkhx6rr%2BN4dvi0iKu3MFkbC6TerRmqoTIuj0G6GD67%2BYcQ7eoSwLc%2BCKY3g9Muzk0RnpEO8a8qFu04g%2BSF%2BaejvDI02liu5Jma%2B3sUqNMGsAlkhyqImFMBxQNdwS9U%2B2Q7uCXwlM8Um9%2FR6Azwc67EVFY74cE%2BfCIhsax6pp0oVORF422R08El33y%2FtldxlJ83g6zCwYuxG7Ki07xJgINxa18TegHJN5SMbF3FP4R%2F4ukrL9%2Bm38Ul33y%2FtldxlyN9XiUrsjYdNTp1ZJtc2t%2F0sAryYjmPgfKBJ%2BzweHN%2BEjGYASbP510ihKTbTX8AJ%2Bconsld76auYZBQ7Mzm8kuMqtRKg%2B0MAatrKZA0hPQFq1Mg8d4EIkm2bHQ363PUdpA6UnDbnnrMTHGsvLcogGdH23OgmI4MVFYD6YI0CBdRe5m%2F33Dizq1cgI5wUTiqAaU3Yod0t%2B2kPWGvktVM8iXuDvtzEu7evCL4%2BNzVxHZ3cy2iNyXBBNsP9panAwvo72THJzOWuYo32Vjs0DqUkMCo%2Bl4GSWkMxqczj3j%2F5Db9VoSLoKnIBYCH0GFPLBvFNGaXGPGtH4taFwWjI4tlhCXB%2F2G1EbunKUZqCSh1lsVyneJrWEkl4nCo%2Bl4GSWkMxQkqLEYPXz4MIvj43NXEdnSH0GFPLBvFNgqTL%2Bd2f2giqHOkwI5xQtGt9da3jxDKgX2pFqXfDV6AHpt1n3zFGZLkCjg1atLHbL6xsSk3w4dhr%2BQiKZcBDxVWhIugqcgFgIfQYU8sG8U2r6LvfP3jJjjJPlEXClY%2BZFX9I7qIl5wTLecZb7MB%2FqCo%2Bl4GSWkMxlKJmFIveKE4Ivj43NXEdncjEHMsi1ggf6seDUKS9UDsLdUOF0MlSLwem3WffMUZky3nGW%2BzAf6hI%2BCfDjf%2BRRjRba3e5y7js5xwzi18sQUi8z80q9rhSqr4sDZxMZ1p7QmJl3PJjf422Wa04%2FYLuTEeavNkOz1IUhL0pG3UVViQ6Py8J1nrq8w%3D%3D&websig=e778fce2a3d7b4644eb31496c55b40c4fc5f57ca9db76f7ca84402b4b09f7661b105157113008a8aaa15fcd8100f399e6bd852e738a008fdad28efd8eef61312&cdata=82&fpinfo=undefined&eks=_a93d9fb33c6b6073285a1b20ba4f8bbe04e082aa50ed154b7e0f5bfa7fee4519c580c9513878028687d7cc38006af4e49a2834bbfa211cfe1bf52590eec7785fd9b7f4268b91b67b5d2515b90c1392647cacd1a30ffc5b156a9bab7535cb135c631f52a7ca47d2b86857cbe1192b1fc1dc8cf5031aa191ed577e539c711c1b91a9a958b3739b9106b331c0bfc8ff352467ba5ee67e2553c3338949ff17aae567acfe35ce9a13e463e8dcf46fc4e0b47b366008fe981b294ea070492b8b9f22156e07632f632c812f&tlg=1".format(asig,ua,sess,sid)

        result = requests.post(url, data=postdata, headers=headers, timeout=10)
        r=result.text
        j_str = json.loads(r)
        errorCode=j_str["errorCode"]
        if(errorCode==0):
            print("识别成功")
            ticket = j_str["ticket"]
        #

    # print(result.text)

def getVideoInfo():
    '''获取作品数据'''
    url = "http://m.gifshow.com/rest/kd/feed/profile"
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json; charset=UTF-8",
        "kpf": "H5",
        "kpn": "KUAISHOU",
        "Origin": "http://m.gifshow.com",
        "Cookie": "did=web_364d45126146cddf443adf{};".format(random.randint(1000000000, 9999999999)),
        "User-Agent": "Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Mobile Safari/537.36",
    }
    data = {"eid": "bihukankan", "count": 18, "pcursor": ""}
    headers["Cookie"] = "didv=1588123703000; did=web_25da43547ba7474180bc88435b3494bd;"
    result = requests.post(url, headers=headers, data=json.dumps(data), timeout=10).json()
    print(result)

if __name__ == "__main__":
    # getWebUserInfo()
    # getH5UserInfo("bihukankan")
    # getVideoInfo()
    getShopList2()