import urllib

import requests,re,random,json
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
import time
from io import BytesIO
from PIL import Image
from selenium.webdriver import ActionChains
from selenium.common.exceptions import TimeoutException
import random

# import cv2



class CrackGeetest():
    def __init__(self):
        #初始化
        option = webdriver.ChromeOptions()
        option.add_argument('disable-infobars')
        # 改动打开浏览器
        self.url = 'https://www.kwaishop.com/merchant/shop/list?id=330143373'
        self.browser = webdriver.Chrome("/Users/yangfan/Downloads/chromedriver",options=option)
        self.browser.maximize_window()
        self.wait = WebDriverWait(self.browser, 5)


    def open(self):
        self.browser.get(self.url)
        self.browser.implicitly_wait(3)
        self.browser.switch_to.frame("tcaptcha_iframe")

    def setDriver(self,driver):
        self.browser=driver


    def get_geetest_button(self):
        """[summary]

        获取初始验证按钮
        返回按钮对象
        """
        button = self.wait.until(EC.element_to_be_clickable((By.ID, 'slide_bar_head')))
        print("获取初始验证按钮")
        return button

    def get_position(self):
        """[summary]

        获取验证码位置-为截取验证码准备
        返回验证码位置元组
        """
        img = self.wait.until(EC.presence_of_element_located((By.CLASS_NAME, 'drag')))
        print(img)
        time.sleep(2)
        location = img.location
        size = img.size
        top, bottom, left, right = location['y'], location['y'] + size['height'], location['x'], location['x'] + size[
            'width']
        return (top, bottom, left, right)

    def get_screenshot(self):
        """[summary]

        获取网页截图
        """
        screenshot = self.browser.get_screenshot_as_png()
        screenshot = Image.open(BytesIO(screenshot))
        return screenshot

    def get_geetest_image(self, name):
        """[summary]

        获取验证码图片
        返回图片对象
        """
        top, bottom, left, right = self.get_position()
        print("top=",top,"bottom",bottom,"left",left,"right=",right)
        screenshot = self.get_screenshot()
        # 通过验证码图片的位置从网页截图上截取验证码
        captcha = screenshot.crop((left, top, right, bottom))
        captcha.save(name)
        #截取图片
        return captcha

    def get_slider(self):
        """[summary]

        获取滑块对象
        """
        slider = self.wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'geetest_slider_button')))
        return slider

    def get_gap(self, image1, image2):
        ''' 获取缺口偏移量, 参数：image1不带缺口图片、image2带缺口图片。返回偏移量 '''
        left = 65
        for i in range(left, image1.size[0]):
            for j in range(image1.size[1]):
                if not self.is_pixel_equal(image1, image2, i, j):
                    left = i
                    return left
        return left

    def get_track(self, distance):
        """[summary]

        根据偏移量获取移动轨迹

        Arguments:
            distance {[type]} -- 偏移量
        """
        # 移动轨迹
        track = []
        # 当前位移
        current = 0
        # 减速阀值
        mid = distance * 7 / 10
        # 计算间隔
        t = 0.15
        # 初速度
        v = 0

        while current < distance:
            if current < mid:
                # 加速度为正
                a = 2.1
            else:
                # 加速度为负
                a = -4.8
            # 初速度v0
            v0 = v
            # 当前速度 v = v0 + at
            v = v0 + a * t
            # 移动距离 x = v0t + 1/2*a*t*t
            move = v0 * t + 1 / 2 * a * t * t
            # 当前位移
            current += move
            # 加入轨迹
            track.append(round(move, 2))
        return track

    def move_to_gap(self, slider, tracks):
        """[summary]

        拖动滑块到缺口处
        动作： 点击且不释放鼠标-拖拽图片到缺口-释放鼠标

        Arguments:
            slider {[type]} -- 滑块
            tracks {[type]} -- 轨迹
        """
        # 点击并按住滑块
        ActionChains(self.browser).click_and_hold(slider).perform()
        # 移动
        for x in tracks:
            ActionChains(self.browser).move_by_offset(xoffset=x, yoffset=0).perform()
        time.sleep(0.1)
        # 释放滑块
        ActionChains(self.browser).release().perform()

    # def login(self):
    #     """[summary]
    #
    #     登录
    #     """
    #     submit = self.wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'login-btn')))
    #     submit.click()
    #     time.sleep(1)
    #     print('登录成功')

    def is_try_again(self):
        """[summary]

        判断是否能够点击重试
        """
        button_text = self.browser.find_element_by_class_name('geetest_radar_tip_content')
        text = button_text.text
        if text == '尝试过多':
            button = self.browser.find_element_by_class_name('geetest_reset_tip_content')
            button.click()

    def is_success(self):
        """[summary]

        判断是否成功
        """
        button_text2 = self.browser.find_element_by_class_name('geetest_success_radar_tip_content')
        text2 = button_text2.text
        if text2 == '验证成功':
            return 1
        return 0

    def for_move(self, x_temp):
        """[summary]

        循环拖动
        """
        flag = 0
        for i in range(0, 7):

            gap = random.randint(16, 20) * i + x_temp
            if gap < 40:
                continue
            print('预估计缺口位置: ', gap)
            self.is_try_again()
            slider = self.get_slider()
            track = self.get_track(gap)
            print('滑动轨迹: ', track)
            # 拖动滑块
            self.move_to_gap(slider, track)
            time.sleep(3)
            if self.is_success():
                flag = 1
                break
        return flag

    def move_to(self,brower,element, x):

        # element = brower.find_element_by_xpath(s3)
        ActionChains(brower).click_and_hold(on_element=element).perform()
        ActionChains(brower).move_to_element_with_offset(to_element=element, xoffset=x, yoffset=0).perform()
        time.sleep(3)
        ActionChains(brower).release(on_element=element).perform()

    def crack(self):
        """[summary]
        验证
        """
        try:
            # 打开url
            self.open()
            # 点击验证按钮
            slide_bar_head = self.get_geetest_button()
            # button.click()
            # 获取验证码图片

            # oripic=self.browser.find_element_by_class_name("oripic")
            img=self.browser.find_element_by_css_selector("div > img")
            img_url=img.get_property("src")
            #413 * 236

            with urllib.request.urlopen(
                    img_url, timeout=30) as response, open("captcha.png"
                , 'wb') as f_save:
                f_save.write(response.read())
                f_save.flush()
                f_save.close()
                print("成功")
                #图片识别x
                x = track_code()
                print(x)

                self.move_to(self.browser, slide_bar_head, x)





            # image1 = self.get_geetest_image('captcha.png')
            # print(image1)
            # # 获取带缺口的验证码



            # flag = 0
            # while 1:
            #     temp = random.randint(0, 2)  # 将轨迹划分为2, 则有1/2的几率
            #     if temp == 0:
            #         print('预估左1/2: ')
            #         flag = self.for_move(30)
            #     else:
            #         print('预估右1/2: ')
            #         flag = self.for_move(120)
            #     if flag == 1:
            #         break

        except TimeoutException as e:
            print("异常",e)
            # self.crack()
        # 成功,登录
        # self.login()
        # time.sleep(10)

def track_code():
     #缩放 413 * 236

     # img = cv2.imread('captcha.png', 3)
     #
     # res = cv2.resize(img, (413, 236), interpolation=cv2.INTER_CUBIC)
     # cv2.imwrite('captcha.png', res)

     img=Image.open("captcha.png")
     out = img.resize((413, 236))

     out.save("captcha.png")

     #灰度化

     Image.open('captcha.png').convert('L').save('cap4.jpg')
     # im=Image.open("cap4.jpg")

     x=convert('cap4.jpg')
     print("x=",x)
     return  x

def convert(imgName):
    im = Image.open(imgName)
    im = im.convert('RGBA')

    width = im.size[0]
    height = im.size[1]
    listx=[]
    for h in range(0, height):
        for w in range(0, width):
            pixel = im.getpixel((w, h))
            if(width-w>2):
                pixel2 = im.getpixel((w + 2, h))
                sr=pixel[0]-pixel2[0]
                sg = pixel[1] - pixel2[1]
                sb = pixel[2] - pixel2[2]
                if sr>110 and sg >110 and sb>110:
                    print("r", sr, "g", sg, "b", sb)
                    if len(listx)>0:
                        listxitem = set(listx)
                        for item in listxitem:
                            # print(item)
                            if listx.count(item)>20:
                                return item
                    listx.append(w)

            # if pixel[0] > 0 and pixel[1] > 0 and pixel[2] > 0 and pixel[3] > 0:
            #     im.putpixel((w, h), (0,0,0, 220))
            # elif pixel[0] == 0 and pixel[1] == 0 and pixel[2] == 0:
            #     im.putpixel((w, h), (0, 0, 0, 220))
            # else:
            #     pass
        #连续8次 横坐标一样判断位滑动的位置

    # im.save(saveName)



if __name__ == "__main__":
    # test()
    crack = CrackGeetest()
    crack.crack()


