import binascii

from Util import SecurityUtil
from Util import TimeUtil
import numpy as np;
from functools import reduce

# Xbox_=[]

#二进制转十进制
def twototen(bin):
    # bin = str(input('输入二进制数：'))
    count = 0
    for i in range(0, len(bin)):
        if bin[i] == str(1):
            sum = 2 ** (len(bin) - i - 1)
            count = count + sum
    # print count
    return count

#获得十进制转二进制的字符串
def b2(str1):
    bi2=str(bin(int(str1))).replace('0b','')
    return bi2

# 2进制低位抹除
def lowbit(r):
    s = b2(r)
    if(len(s)>32):
        #获取32个
        s = s[len(s)-32:len(s)]
        # print(s)
        s=  s[s.index("1"):len(s)]

    result = twototen(s)
    return result

def UBFX(Rn,lsb,width):
    bit_string = '{:0%db}' % 32
    # 十进制 转2进制
    e = bit_string.format(Rn)

    while(len(e)<lsb+width):
        e="0"+e

    e=e[len(e)-width-lsb:len(e)-lsb]
    e = int(e, 2)

    # print(str(e))
    return e

#加法
def ADD(a,b):
    r=a+b
    r1=lowbit(r)
    return r1;

#位异或
def Long_Xor(a,b):
    return  a^b;

#循环右移
def rorCall(int_value, k, bit=32):
        bit_string = '{:0%db}' % bit

        # 十进制 转2进制
        bin_value = bit_string.format(int_value)  # 8 bit binary
        # print(bin_value)
        bin_value = bin_value[-k:] + bin_value[:-k]

        int_value = int(bin_value, 2)
        # print(int_value)
        return int_value

#循环左移
def lorCall(a,b):
    return a[b:] + a[:b]

def Long_Or(a,b):
    return a|b;

#取反
def LongNot(a):
    return ~a;

#右移动
def Long_Sar(a,b):
    return a>>b;

#位与
def Long_And(a,b):
    return a&b;

#位取反
def Long_Not(a):
    return ~a;

#十六进制转十进制
def hextoint(a):
    if(a==''):
        return 0
    return int(a,16)

# def inttohex(data):
#     lin = ['%02X' % i for i in data]
#     print("".join(lin))

#十进制转十六进制
def inttohex(a):
    r=hex(a)[2:]
    if(len(r)<8):
        r='0'+r
    return r
    # return hex(a)

#与运算
def AND(a,b):
    return a&b;


def getlist2(list2,num):
    list_len=len(list2)
    i=0
    num=num-list_len

    while(i<num):
        list2.append(0)
        i+=1

    return list2


def getlist(list,num):
    list_len=len(list)
    i=0
    num=num-list_len
    r_list=[]
    # list=[]
    while(i<num):
        r_list.append(0)
        i+=1

    return r_list

# def getlist(num):
#     i=0
#     list=[]
#     while(i<num):
#         list.append(0)
#
#     return list

def long_SHA(data):
    print(data)
    R8=[]
    R10=[]
    R8_=[]
    R10_=[]
    SHA256_BOX=[1116352408, 1899447441, 3049323471, 3921009573, 961987163, 1508970993, 2453635748, 2870763221,
                  3624381080, 310598401, 607225278, 1426881987, 1925078388, 2162078206, 2614888103, 3248222580,
                  3835390401, 4022224774, 264347078, 604807628, 770255983, 1249150122, 1555081692, 1996064986,
                  2554220882, 2821834349, 2952996808, 3210313671, 3336571891, 3584528711, 113926993, 338241895,
                  666307205, 773529912, 1294757372, 1396182291, 1695183700, 1986661051, 2177026350, 2456956037,
                  2730485921, 2820302411, 3259730800, 3345764771, 3516065817, 3600352804, 4094571909, 275423344,
                  430227734, 506948616, 659060556, 883997877, 958139571, 1322822218, 1537002063, 1747873779, 1955562222,
                  2024104815, 2227730452, 2361852424, 2428436474, 2756734187, 3204031479, 3329325298]

    data_len=len(data)
    v255=[1901292640, 1130259040, 1918175103, 1946289006, 1886916213, 1598096728, 1483105631, 1283015438, 2054642800,2071427919, 1635081840, 1115188739, 255881028, 58224386, 41158415, 1131698180]
    # v255=getlist2(v255,64)

    print(v255)
    v14=0
    v15=v255[v14]
    v13=v15
    v16=0

    print(v255)
    while(v14!=48):
        a=v255[v14+ 14]
        b=v255[v14+9]
        v16=v255[v14+1]
        v255.append(ADD(v13, ADD(b, ADD(Long_Xor(Long_Xor(rorCall(v16, 18), Long_Sar(v16, 3)), rorCall(v16, 7)),Long_Xor(Long_Xor(rorCall(a, 19), Long_Sar(a, 10)), rorCall(a, 17))))))
        v13=v16
        v14=v14+1

    v19=1013904242
    v17=3144134277
    v21=528734635
    v18=1779033703
    v22=2600822924
    v23=1359893119
    v176=1541459225
    v182=2773480762

    R8.append(v18)
    R8.append(v17)
    R8.append(v19)
    R8.append(v182)
    R8.append(v23)
    R8.append(v22)
    R8.append(v21)
    R8.append(v176)

    #第一次计算
    print('第一次计算',v255)
    # print('len=',len(v255))


    v20=0
    while(v20!=64):

        v24 = v19
        v25 = v17
        v26 = v21
        v27 = v18
        v28 = v22
        v29 = v255[v20]
        v30 = v23
        v31 = Long_Xor(Long_And(Long_Xor(v17, v24), v18), Long_And(v17, v24))
        v32 = SHA256_BOX[v20]
        V33 = ADD(ADD(ADD(Long_Xor(Long_And(v21, Long_Not(v30)), Long_And(v30, v28)), v176),
                      Long_Xor(Long_Xor(rorCall(v30, 6), rorCall(v30, 11)), rorCall(v30, 25))), v32)
        v17 = v18
        V34 = ADD(V33, v29)
        v22 = v30
        v18 = ADD(ADD(v31, Long_Xor(Long_Xor(rorCall(v18, 2), rorCall(v18, 13)), rorCall(v18, 22))), V34)
        v23 = ADD(V34, v182)
        v21 = v28
        v19 = v25
        v176 = v26
        v182 = v24
        v20 = v20 + 1


    R10.append(v18)
    R10.append(v27)
    R10.append(v25)
    R10.append(v24)
    R10.append(v23)
    R10.append(v30)
    R10.append(v28)
    R10.append(v26)

    print('第二次计算 R10',R10)



    i=0
    while(i<8):
        v36 = R8[i]
        R8[i] = ADD(R10[i], v36)
        i+=1

    print('第一轮结束')

    if(data_len>63):
       ## 重定义数组(v255, 假, 64)
       # 清除数组(R10)
       list_arr=[]
       v255=getlist(list_arr,64)
       R10.clear()

       lend = bytes(data[0:64],encoding="utf-8").hex()
       # ' 调试输出 (取文本左边 (“sxCm4/ZOVjSENhlpdWkwHhJQidOwJjgdjm005MAlkejjsJLzqrC6FGRRlwHcxhUvn/Nei+EMtuZz69NF0QYblM3pwjJu4D3fk7GNCO5pbQY=”, 64))
       i=0

       while(i<16):
           v255[i] = hextoint(lend[(i* 8):(i* 8+8)])
           i+=1

       v14 = 0
       v15 = v255[v14]
       v13 = v15

       while(v14!=48):
           a = v255[v14 + 14]
           b = v255[v14 + 9]
           v16 = v255[v14 + 1]
           v255[v14 + 16] = ADD(v13, ADD(b, ADD(Long_Xor(Long_Xor(rorCall(v16, 18), Long_Sar(v16, 3)), rorCall(v16, 7)),
                                                Long_Xor(Long_Xor(rorCall(a, 19), Long_Sar(a, 10)), rorCall(a, 17)))))
           v13 = v16
           v14 = v14 + 1

       v19 = R8[1]
       v17 = R8[2]
       v21 = R8[6]
       v18 = R8[0]
       v22 = R8[5]
       v23 = R8[4]
       v176 = R8[7]
       v182 = R8[3]
       v20 = 0

       v183 = v18
       V45 = v19
       V44 = v17
       V48 = v21
       v177 = v176
       v50 = v23
       v47 = v182
       v49 = v22

       while(v20!=64):
           v51 = v183
           v52 = V45
           v53 = V44
           v54 = V48
           v55 = v49
           v56 = v47
           v57 = v50
           v58 = ADD(Long_Xor(Long_Xor(rorCall(v183, 2), rorCall(v183, 13)), rorCall(v183, 22)),
                     Long_Xor(Long_And(V45, V44), Long_And(Long_Xor(V45, V44), v183)))
           V59 = v255[v20]
           v60 = Long_Xor(Long_And(V48, Long_Not(v57)), Long_And(v57, v55))
           v61 = v177
           v177 = v54
           v62 = SHA256_BOX[v20]
           v63 = ADD(
               ADD(ADD(ADD(Long_Xor(Long_Xor(rorCall(v57, 6), rorCall(v57, 11)), rorCall(v57, 25)), v61), v60), v62),
               V59)
           V48 = v55
           v64 = ADD(v58, v63)
           v50 = ADD(v63, v56)
           v183 = v64
           v47 = v53
           v49 = v57
           V44 = v52
           V45 = v51
           v20 = v20 + 1

       R10.append(v183)
       R10.append(v51)
       R10.append(V44)
       R10.append(v47)
       R10.append(v50)
       R10.append(v49)
       R10.append(V48)
       R10.append(v54)

       i=0
       while(i<8):
           v36 = R8[i]
           R8[i] = ADD(R10[i], v36)
           i+=1

       data =data[(data_len-64):]
       # 取文本右边(data, data_len-64)
       #第二轮结束(第二轮只处理长度>63)


    print('第二轮结束 v255',v255)
    print('第二轮结束 R10', R10)

    V66 = AND(data_len, 63)
    list_arr=[]
    v255=getlist(list_arr,64)
   # 重定义数组(v255, 假, 64)

    R10.clear()
    em=[128, 0, 0, 0]
    print(data)
    v223 =list(bytes(data,encoding="utf-8"))
    v223.extend(em)

    # y = str(bytearray(v223))
    lend=bytearray(v223).hex()
    # lend =binascii.b2a_hex(y)

    # lend=v223.hex()


    if(V66>55):
        i=0
        while(i<16):
            v255[i] = hextoint(lend[(i * 8 + 1):(i * 8 + 9)])
            i+=1

        v14 = 0
        v15 = v255[v14]
        v13 = v15


        while(v14!=48):
            a = v255[v14 + 14]
            b = v255[v14 + 9]
            v16 = v255[v14 + 1]
            print('11',v14+16)
            v255[v14 + 16] = ADD(v13, ADD(b, ADD(Long_Xor(Long_Xor(rorCall(v16, 18), Long_Sar(v16, 3)), rorCall(v16, 7)),
                                                 Long_Xor(Long_Xor(rorCall(a, 19), Long_Sar(a, 10)), rorCall(a, 17)))))
            v13 = v16
            v14 = v14 + 1

        v19 = R8[3]
        v17 = R8[2]
        v21 = R8[7]
        v18 = R8[1]
        v22 = R8[6]
        v23 = R8[5]
        v176 = R8[8]
        v182 = R8[4]
        v20 = 0

        if(v20!=64):
            v24 = v19
            v25 = v17
            v26 = v21
            v27 = v18
            v28 = v22
            v29 = v255[v20]
            v30 = v23
            v31 = Long_Xor(Long_And(Long_Xor(v17, v24), v18), Long_And(v17, v24))
            v32 = SHA256_BOX[v20]
            V33 = ADD(ADD(ADD(Long_Xor(Long_And(v21, Long_Not(v30)), Long_And(v30, v28)), v176),
                          Long_Xor(Long_Xor(rorCall(v30, 6), rorCall(v30, 11)), rorCall(v30, 25))), v32)
            v17 = v18
            V34 = ADD(V33, v29)
            v22 = v30
            v18 = ADD(ADD(v31, Long_Xor(Long_Xor(rorCall(v18, 2), rorCall(v18, 13)), rorCall(v18, 22))), V34)
            v23 = ADD(V34, v182)
            v21 = v28
            v19 = v25
            v176 = v26
            v182 = v24
            v20 = v20 + 1

        R10.append(v18)
        R10.append(v27)
        R10.append(v25)
        R10.append(v24)
        R10.append(v23)
        R10.append(v30)
        R10.append(v28)
        R10.append(v26)

        i=0
        while(i<8):
            v36 = R8[i]
            R8[i] = ADD(R10[i], v36)

        R10.clear()
        lend = lend[(len(lend)-128):]






    print('第三次运算 R10',R10)
    print('第三次运算 v255', v255)
    # print('第三次运算 lend', lend)

    #重定义数组
    list_arr=[]
    v255=getlist(list_arr,64)

    print('leng=',lend)
    i=0
    while(i<16):
        v255[i] = hextoint(lend[(i* 8): (i* 8 +8)])
        i+=1



    v255[15] = data_len * 8 + 512
    v14 = 0
    v15 = v255[v14]
    v13 = v15


    print('第30次计算 v255', v255)


    while(v14!=48):
        a = v255[v14 + 14]
        b = v255[v14 + 9]
        v16 = v255[v14 + 1]
        v255[v14 + 16] = ADD(v13, ADD(b, ADD(Long_Xor(Long_Xor(rorCall(v16, 18), Long_Sar(v16, 3)), rorCall(v16, 7)),
                                             Long_Xor(Long_Xor(rorCall(a, 19), Long_Sar(a, 10)), rorCall(a, 17)))))
        v13 = v16
        v14 = v14 + 1


    v19 = R8[2]
    v17 = R8[1]
    v21 = R8[6]
    v18 = R8[0]
    v22 = R8[5]
    v23 = R8[4]
    v176 = R8[7]
    v182 = R8[3]
    v20 = 0


    print('第4次计算 R10', R10)
    print('第4次计算 v255', v255)


    while(v20!=64):
        v24 = v19
        v25 = v17
        v26 = v21
        v27 = v18
        v28 = v22
        v29 = v255[v20]
        v30 = v23
        v31 = Long_Xor(Long_And(Long_Xor(v17, v24), v18), Long_And(v17, v24))
        v32 = SHA256_BOX[v20]
        V33 = ADD(ADD(ADD(Long_Xor(Long_And(v21, Long_Not(v30)), Long_And(v30, v28)), v176),
                      Long_Xor(Long_Xor(rorCall(v30, 6), rorCall(v30, 11)), rorCall(v30, 25))), v32)
        v17 = v18
        V34 = ADD(V33, v29)
        v22 = v30
        v18 = ADD(ADD(v31, Long_Xor(Long_Xor(rorCall(v18, 2), rorCall(v18, 13)), rorCall(v18, 22))), V34)
        v23 = ADD(V34, v182)
        v21 = v28
        v19 = v25
        v176 = v26
        v182 = v24
        v20 = v20 + 1

    R10.append(v18)
    R10.append(v27)
    R10.append(v25)
    R10.append(v24)
    R10.append(v23)
    R10.append(v30)
    R10.append(v28)
    R10.append(v26)

    print('第5次计算 R10', R10)
    print('第5次计算 v255', v255)


    i=0
    while(i<8):
        v36 = R8[i]
        R8[i] = ADD(R10[i], v36)
        i+=1

     # 这里的R8是下一伦加密的值
     # v255 = { 623908408, 873992037, 788934431, 489646645, 991826702, 1852598549, 691078765, 957244461, 254164537, 839132676, 1868895775, 202514287, 134560008, 188221797, 202325265, 1848382743 }’这是极速版
     # v255 = { 460141321, 134964260, 605907054, 302803502, 137193786, 857629726, 403708520, 925639989, 773786985, 154740287, 353515310, 1684932100, 421016115, 1694838838, 439904314, 302323310 }‘6.7.1.10332

    v255 = [456721930, 691275786, 406809877, 510159108, 437412895, 892038962, 839651125, 638789988, 270349850, 287117605,
        186594330, 672269417, 1697252654, 1762788712, 1746824549, 689844846]

   # 重定义数组(v255, 真, 64)
    v255=getlist2(v255,64)
    v14 = 0
    v15 = v255[v14]
    v13 = v15

    # print('第六次开始',v255)

    while(v14!=48):
        a = v255[v14 + 14]
        b = v255[v14 + 9]
        v16 = v255[v14 + 1]
        v255[v14 + 16] = ADD(v13, ADD(b, ADD(Long_Xor(Long_Xor(rorCall(v16, 18), Long_Sar(v16, 3)), rorCall(v16, 7)),
                                             Long_Xor(Long_Xor(rorCall(a, 19), Long_Sar(a, 10)), rorCall(a, 17)))))
        v13 = v16
        v14 = v14 + 1

    v19 = 1013904242
    v17 = 3144134277
    v21 = 528734635
    v18 = 1779033703
    v22 = 2600822924
    v23 = 1359893119
    v176 = 1541459225
    v182 = 2773480762
    v20 = 0


    R8_.append(v18)
    R8_.append(v17)
    R8_.append(v19)
    R8_.append(v182)
    R8_.append(v23)
    R8_.append(v22)
    R8_.append(v21)
    R8_.append(v176)

    print('第六次计算 v255',v255)
    print('第六次计算 R8_', R8_)

    while(v20!=64):
        v24 = v19
        v25 = v17
        v26 = v21
        v27 = v18
        v28 = v22
        v29 = v255[v20]
        v30 = v23
        v31 = Long_Xor(Long_And(Long_Xor(v17, v24), v18), Long_And(v17, v24))
        v32 = SHA256_BOX[v20]
        V33 = ADD(ADD(ADD(Long_Xor(Long_And(v21, Long_Not(v30)), Long_And(v30, v28)), v176),
                      Long_Xor(Long_Xor(rorCall(v30, 6), rorCall(v30, 11)), rorCall(v30, 25))), v32)
        v17 = v18
        V34 = ADD(V33, v29)
        v22 = v30
        v18 = ADD(ADD(v31, Long_Xor(Long_Xor(rorCall(v18, 2), rorCall(v18, 13)), rorCall(v18, 22))), V34)
        v23 = ADD(V34, v182)
        v21 = v28
        v19 = v25
        v176 = v26
        v182 = v24
        v20 = v20 + 1


    R10_.append(v18)
    R10_.append(v27)
    R10_.append(v25)
    R10_.append(v24)
    R10_.append(v23)
    R10_.append(v30)
    R10_.append(v28)
    R10_.append(v26)

    # print('第7次计算 v255', v255)
    print('第7次计算 R10_', R10_)

    i=0
    while(i<8):
        v36 = R8_[i]
        R8_[i] = ADD(R10_[i], v36)
        i+=1
    print('第7次计算 R8_', R8_)

    #开始最后一轮

   # 重定义数组(R8, 真, 64)
    R8=getlist2(R8,64)
    R8[8] = 2147483648
    R8[15] = 768
    lend =''



    R10.clear()
    v14 = 0
    v15 = R8[v14]
    v13 = v15


    while(v14!=48):
        a = R8[v14 + 14]
        b = R8[v14 + 9]
        v16 = R8[v14 + 1]
        R8[v14 + 16] = ADD(v13, ADD(b, ADD(Long_Xor(Long_Xor(rorCall(v16, 18), Long_Sar(v16, 3)), rorCall(v16, 7)),
                                           Long_Xor(Long_Xor(rorCall(a, 19), Long_Sar(a, 10)), rorCall(a, 17)))))
        v13 = v16
        v14 = v14 + 1



    v19 = R8_[2]
    v17 = R8_[1]
    v21 = R8_[6]
    v18 = R8_[0]
    v22 = R8_[5]
    v23 = R8_[4]
    v176 = R8_[7]
    v182 = R8_[3]
    v20 = 0

    while(v20!=64):
        v24 = v19
        v25 = v17
        v26 = v21
        v27 = v18
        v28 = v22
        v29 = R8[v20]
        v30 = v23
        v31 = Long_Xor(Long_And(Long_Xor(v17, v24), v18), Long_And(v17, v24))
        v32 = SHA256_BOX[v20]
        V33 = ADD(ADD(ADD(Long_Xor(Long_And(v21, Long_Not(v30)), Long_And(v30, v28)), v176),
                      Long_Xor(Long_Xor(rorCall(v30, 6), rorCall(v30, 11)), rorCall(v30, 25))), v32)
        v17 = v18
        V34 = ADD(V33, v29)
        v22 = v30
        v18 = ADD(ADD(v31, Long_Xor(Long_Xor(rorCall(v18, 2), rorCall(v18, 13)), rorCall(v18, 22))), V34)
        v23 = ADD(V34, v182)
        v21 = v28
        v19 = v25
        v176 = v26
        v182 = v24
        v20 = v20 + 1



    R10.append(v18)
    R10.append(v27)
    R10.append(v25)
    R10.append(v24)
    R10.append(v23)
    R10.append(v30)
    R10.append(v28)
    R10.append(v26)


    print('第8次计算 R8', R8)
    print('第8次计算 R10', R10)


    hex =''
    i=0
    while(i<8):
        v36 = R8_[i]
        R8_[i] = ADD(R10[i], v36)
        hex = hex + inttohex(R8_[i])
        i+=1
    print('hex=',hex)
    return hex
# 返回(到小写(hex))

def transformInt2(str):
    indexTable = [0, 5, 10, 15, 4, 9, 14, 3, 8, 13, 2, 7, 12, 1, 6, 11]
    temp=[]
    # temp=getlist2(temp,16)
    i=0
    while(i<16):
        temp.append(str[indexTable[i]])
        i+=1

    print(temp)
    return temp

def transformInt(str):
    indexTable = [0, 5, 10, 15, 4, 9, 14, 3, 8, 13, 2, 7, 12, 1, 6, 11]
    temp=[]
    # temp=getlist2(temp,16)
    i=0
    while(i<16):
        temp.append(ord(str[indexTable[i]]))
        i+=1

    print(temp)
    return temp

def initsig3(str):
    #with open 打开之后自动关闭
    with open('xbox_','r+') as rf:
        #读取第一行
        result=rf.read()
        global Xbox_
        Xbox_=result.split(' ')

        # print(Xbox_)
        r = getsig3(str)
        print('result=', r)

    return

def befor_Base64(input):

    salt=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 1, 0, 3, 2, 5, 4, 7, 6, 9, 8, 11, 10, 13, 12, 15, 14, 2, 3, 0, 1, 6, 7, 4, 5, 10, 11, 8, 9, 14, 15, 12, 13, 3, 2, 1, 0, 7, 6, 5, 4, 11, 10, 9, 8, 15, 14, 13, 12, 4, 5, 6, 7, 0, 1, 2, 3, 12, 13, 14, 15, 8, 9, 10, 11, 5, 4, 7, 6, 1, 0, 3, 2, 13, 12, 15, 14, 9, 8, 11, 10, 6, 7, 4, 5, 2, 3, 0, 1, 14, 15, 12, 13, 10, 11, 8, 9, 7, 6, 5, 4, 3, 2, 1, 0, 15, 14, 13, 12, 11, 10, 9, 8, 8, 9, 10, 11, 12, 13, 14, 15, 0, 1, 2, 3, 4, 5, 6, 7, 9, 8, 11, 10, 13, 12, 15, 14, 1, 0, 3, 2, 5, 4, 7, 6, 10, 11, 8, 9, 14, 15, 12, 13, 2, 3, 0, 1, 6, 7, 4, 5, 11, 10, 9, 8, 15, 14, 13, 12, 3, 2, 1, 0, 7, 6, 5, 4, 12, 13, 14, 15, 8, 9, 10, 11, 4, 5, 6, 7, 0, 1, 2, 3, 13, 12, 15, 14, 9, 8, 11, 10, 5, 4, 7, 6, 1, 0, 3, 2, 14, 15, 12, 13, 10, 11, 8, 9, 6, 7, 4, 5, 2, 3, 0, 1, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
    BOX=[182,240,201,222,180,10,105,144,211,58,242,186,233,161,96,65,190,243,115,118,166,67,235,185,21,135,143,37,61,44,205,113,226,56,60,212,73,126,111,207,1,171,228,241,137,159,77,167,151,215,200,165,3,251,193,71,89,120,32,4,227,101,74,192,154,16,230,39,38,112,69,178,217,11,177,107,25,213,23,131,136,108,43,51,176,152,204,20,250,174,229,163,91,147,221,203,24,248,99,114,133,179,54,68,42,116,196,57,35,85,76,188,53,249,29,187,22,52,236,162,128,75,18,197,239,12,33,160,36,122,83,219,6,172,157,134,202,198,95,104,194,209,206,214,218,9,49,109,223,121,0,2,15,244,87,93,234,47,103,208,100,117,220,184,156,8,153,92,72,146,155,97,90,80,26,139,62,138,70,216,31,191,170,79,45,59,181,55,168,106,169,110,142,78,132,41,130,150,255,30,123,195,182,13,247,46,164,183,129,225,231,98,245,102,199,148,19,81,141,246,28,124,254,64,173,145,253,5,50,84,232,210,175,224,238,40,158,66,125,86,7,82,189,127,94,27,17,140,119,48,63,88,149,14,252,34,237,26,229,196,5,77,30,86,158,119,52,205,174,16,122,109,84,70,213,105,136,153,129,43,35,177,29,79,231,2,210,215,87,51,3,233,59,45,85,64,15,165,107,203,218,237,112,152,156,62,100,238,193,71,160,132,220,253,227,101,95,167,1,108,115,44,39,179,113,189,207,21,175,125,22,225,212,130,131,66,180,188,111,121,55,255,7,65,10,94,176,104,60,20,151,143,200,145,24,232,241,135,157,96,208,142,224,146,23,33,214,199,92,128,4,133,168,75,97,182,239,36,6,72,144,178,31,185,93,126,114,106,117,102,204,251,98,110,34,57,8,162,127,247,222,192,116,195,139,78,249,243,80,171,166,164,221,123,201,149,173,154,47,190,244,254,197,63,54,236,248,61,172,56,28,120,209,42,202,13,206,12,147,17,159,137,235,14,27,187,124,226,46,37,19,0,138,83,169,18,103,223,186,91,50,38,141,32,234,9,228,90,216,184,82,41,245,183,48,99,194,81,198,67,69,163,242,217,230,58,140,74,68,11,118,76,240,150,161,89,53,73,134,88,170,49,252,155,148,211,40,181,191,250,219,25,246,14,62,212,6,16,104,125,50,152,86,246,231,208,77,165,161,3,89,211,252,122,157,185,225,192,222,88,98,154,60,81,78,39,216,249,56,112,35,107,163,74,9,240,147,45,71,80,105,123,232,84,181,164,188,22,30,140,32,114,218,63,239,234,106,172,37,213,204,186,160,93,237,179,221,175,42,28,235,250,97,189,57,184,149,118,92,139,210,25,59,117,173,143,34,132,96,17,26,142,76,128,242,40,146,64,43,220,233,191,190,127,137,129,82,68,10,194,58,124,55,99,141,85,1,41,170,178,245,167,18,131,201,195,248,2,11,209,197,0,145,5,33,69,236,23,247,48,243,49,174,44,162,180,214,51,38,134,65,223,19,67,79,87,72,91,241,198,95,83,31,4,53,159,66,202,227,253,73,254,182,115,196,206,109,150,155,153,224,70,244,168,144,158,207,228,219,7,177,119,121,54,75,113,205,171,156,100,8,116,187,101,151,12,193,166,169,238,21,136,130,199,230,36,203,24,46,61,183,110,148,47,90,226,135,102,15,27,176,29,215,52,217,103,229,133,111,20,200,138,13,94,255,108,251,126,120,1,107,124,69,102,37,220,191,92,15,71,143,11,244,213,20,19,195,198,70,160,12,94,246,136,144,58,50,87,196,120,153,252,97,137,141,180,122,218,203,60,68,81,30,34,18,248,42,182,16,125,98,236,242,116,78,86,177,149,205,47,117,255,208,147,146,83,165,108,7,240,197,172,222,4,190,61,54,162,96,5,134,158,217,79,161,121,45,238,22,80,27,173,126,104,38,48,199,214,77,159,241,131,6,150,140,113,193,128,9,249,224,163,14,168,76,53,23,89,129,90,112,167,254,145,21,148,185,179,110,230,207,127,51,40,25,119,221,234,115,111,99,123,100,106,216,132,188,186,183,181,204,95,232,226,65,209,101,210,154,41,13,105,192,253,233,44,189,239,212,46,39,139,62,175,229,170,109,243,63,152,250,31,10,29,130,0,142,59,219,28,223,55,156,49,251,206,171,74,35,66,184,3,118,52,2,17,155,64,215,82,84,166,33,114,211,169,67,56,228,24,245,75,201,135,176,72,36,26,103,93,225,43,157,91,85,178,227,200,247,235,202,8,231,194,57,164,174,32,237,138,133,88,151,73,187,49,122,143,119,9,71,204,31,255,184,100,231,24,76,46,192,101,223,205,191,195,1,92,87,50,196,242,243,145,164,13,102,198,159,59,17,245,216,240,116,201,45,194,111,56,224,84,118,16,160,247,237,152,129,225,104,183,44,81,166,226,103,254,144,91,83,233,241,25,248,54,165,167,39,114,162,63,151,193,109,38,238,61,110,180,117,106,149,29,36,96,10,189,222,7,68,244,172,55,208,158,177,78,20,28,3,215,113,21,47,141,147,48,127,93,37,153,75,67,115,232,236,157,0,187,170,213,27,89,133,200,34,42,168,121,148,51,53,33,182,19,178,199,64,98,23,35,217,112,250,85,99,80,154,86,253,43,66,175,202,235,228,65,140,40,218,57,246,105,134,138,171,197,207,163,88,58,52,74,252,169,150,211,130,41,69,230,209,60,128,123,6,131,32,62,137,179,251,176,4,229,221,11,185,212,173,219,214,139,18,22,188,26,5,14,2,135,174,210,15,73,120,30,82,97,239,124,227,125,190,90,186,146,94,203,12,126,107,249,155,79,70,142,181,206,132,234,95,8,161,72,108,77,220,156,136,127,97,231,221,37,131,238,241,188,230,108,67,197,34,6,94,39,233,73,88,111,242,26,30,177,129,107,185,175,215,194,141,51,159,205,101,128,80,85,213,196,87,235,10,27,3,169,161,245,182,79,44,146,248,239,214,152,103,70,135,207,156,212,28,166,132,202,18,48,157,59,223,2,134,7,42,201,227,52,109,12,98,16,149,163,84,69,222,19,154,106,115,5,31,226,82,220,50,234,190,150,21,13,74,62,237,251,181,125,133,195,136,255,148,99,86,0,1,192,54,174,165,49,243,63,77,151,45,11,105,140,153,57,254,96,172,168,72,143,76,142,17,147,29,110,122,191,46,186,158,250,83,24,173,60,118,124,71,189,180,41,36,38,95,249,75,23,47,66,246,65,9,204,123,113,210,236,160,187,138,32,253,117,92,252,240,232,247,228,78,121,224,81,170,55,61,120,89,155,116,203,4,218,40,179,126,25,22,137,244,206,114,20,35,219,183,33,112,91,100,184,14,200,198,53,178,225,64,211,68,193,199,139,102,216,90,58,208,171,119,93,56,217,176,164,15,162,104,167,145,130,8,209,43,144,229,100,47,218,34,92,18,153,74,170,237,49,178,77,25,123,149,48,138,152,234,150,84,9,2,103,145,167,166,196,241,88,51,147,202,110,68,160,141,165,33,156,120,151,58,109,181,1,35,69,245,162,184,205,212,180,61,226,121,4,243,183,50,171,197,14,6,188,164,76,173,99,240,242,114,39,247,106,194,148,56,115,187,104,59,225,32,63,192,72,113,53,95,232,139,82,17,161,249,98,133,203,228,27,65,73,86,130,36,64,122,216,198,101,42,8,112,204,30,22,38,189,185,200,85,238,255,128,78,12,208,157,119,127,253,44,193,102,96,116,227,70,231,146,21,55,66,118,140,37,175,0,54,5,207,3,168,126,23,250,159,190,177,20,217,125,143,108,163,60,211,223,254,144,154,246,13,111,97,31,169,252,195,134,215,124,16,179,132,105,213,46,83,214,117,107,220,230,174,229,81,176,136,94,236,129,248,142,131,222,71,67,233,79,80,91,87,210,251,135,90,28,45,75,7,52,186,41,182,40,235,15,239,199,11,158,89,43,62,172,206,26,19,219,224,155,209,191,10,93,244,29,57,24,137,201,221,92,167,58,48,117,84,150,121,198,9,215,37,190,115,20,27,132,249,195,127,25,46,214,186,44,125,86,105,181,3,197,203,56,191,236,77,222,73,204,202,134,107,213,87,55,221,166,122,80,53,212,189,169,2,175,101,170,156,143,5,220,38,157,232,6,100,129,148,52,243,109,161,165,69,130,65,131,28,158,16,99,119,178,35,183,147,247,94,21,160,49,123,113,74,176,185,36,41,43,82,244,70,26,34,79,251,76,4,193,118,124,223,225,173,182,135,45,240,120,81,241,253,229,250,233,67,116,237,171,137,199,31,61,144,54,210,15,139,10,39,196,238,57,96,1,111,29,152,174,89,72,211,30,151,103,126,8,18,239,95,209,63,231,179,155,24,0,71,51,224,246,184,112,136,206,133,242,153,110,91,13,12,205,59,163,168,60,254,50,64,154,32,114,108,234,208,40,142,227,252,177,235,97,78,200,47,11,83,42,228,68,85,98,255,23,19,188,140,102,180,162,218,207,128,62,146,192,104,141,93,88,216,201,90,230,7,22,14,164,172,248,187,66,33,159,245,226,219,149,106,75,138,194,145,217,17,218,84,214,73,139,72,143,111,107,167,57,254,94,75,174,204,115,122,128,187,177,251,106,223,148,61,89,125,233,120,189,169,21,182,188,11,206,134,49,133,232,208,140,62,152,225,227,238,39,190,137,35,48,47,55,59,155,178,58,231,77,124,103,43,209,222,185,116,239,29,195,12,179,92,158,191,250,240,109,150,1,15,201,127,163,156,183,230,112,28,228,211,181,9,51,78,176,108,23,253,157,31,161,76,0,6,131,20,135,38,117,242,34,87,236,22,207,69,86,96,175,101,200,99,119,30,255,154,153,193,229,2,132,171,33,123,54,41,68,226,26,32,166,184,74,5,16,104,126,172,70,118,217,221,53,168,159,142,46,224,102,110,196,220,205,44,144,3,18,146,151,71,162,10,88,244,219,19,91,8,64,129,160,95,17,40,63,85,235,136,113,50,170,243,36,14,237,192,65,197,24,252,90,247,213,13,67,97,149,37,216,194,180,173,93,212,25,130,147,100,82,215,165,203,79,4,66,186,114,60,42,249,141,202,210,81,121,45,245,27,234,80,138,248,52,246,98,105,241,7,198,199,145,164,83,56,39,138,101,129,62,28,168,112,89,115,215,142,60,184,144,189,238,25,100,255,216,182,47,170,165,191,232,88,32,169,201,208,175,44,240,183,136,102,4,80,63,199,50,121,87,132,15,65,187,186,140,122,46,69,236,217,247,133,151,45,31,20,73,139,57,159,75,84,219,197,103,93,152,127,228,188,92,6,249,214,72,213,164,160,83,157,226,243,109,21,55,120,59,11,3,209,234,58,111,239,37,137,223,119,185,161,27,19,237,126,176,81,66,40,108,85,12,79,150,245,38,117,166,110,221,34,61,252,227,194,206,33,16,235,135,141,196,9,172,163,190,113,146,96,153,174,13,97,78,51,200,116,180,2,124,114,202,155,222,225,254,105,125,123,8,143,250,91,106,128,205,17,220,49,224,98,181,30,210,24,130,231,10,99,145,107,95,42,43,29,178,56,68,131,22,218,211,177,35,54,171,52,167,41,242,18,246,53,36,0,233,64,192,212,148,5,253,198,14,7,23,162,204,134,241,67,149,173,158,147,229,156,193,118,104,203,76,248,179,251,71,154,230,207,26,86,48,1,244,94,90,195,74,70,77,82,86,255,155,191,43,186,127,107,177,184,66,121,115,57,168,29,169,101,251,60,156,137,108,14,24,150,20,139,73,138,77,173,89,112,248,37,143,190,165,233,229,124,75,225,242,237,245,249,42,18,78,252,90,35,33,44,215,116,126,201,12,68,243,71,178,222,38,17,119,203,241,140,195,205,11,189,97,94,117,36,113,158,92,125,56,50,175,84,19,28,123,182,45,223,1,206,109,167,10,161,181,220,61,88,224,149,46,212,13,135,148,162,194,196,65,214,69,228,183,48,114,174,213,63,95,221,99,142,27,31,247,106,93,76,236,34,136,199,210,170,188,110,132,180,244,235,134,32,216,226,100,122,91,3,39,192,70,105,227,185,211,234,253,151,41,74,179,240,25,209,153,202,130,67,98,157,208,80,85,133,96,200,154,54,164,172,6,30,15,238,82,193,219,64,81,166,144,21,103,9,87,231,26,0,118,111,159,22,218,62,152,53,23,207,129,163,104,49,230,204,47,2,131,7,51,197,4,5,83,102,145,250,40,146,72,58,246,52,160,171,79,8,16,147,187,239,55,217,141,198,128,120,176,254,232,59,149,69,64,192,38,138,216,112,14,22,188,180,209,66,254,31,135,237,250,195,224,163,90,57,218,137,193,9,141,114,83,146,48,150,251,228,106,116,242,200,208,55,19,75,169,243,121,86,122,231,15,11,50,252,92,77,186,194,215,152,164,148,126,172,131,0,24,95,201,39,255,171,104,144,214,157,43,248,238,160,21,20,213,35,234,129,118,67,42,88,130,56,187,176,36,230,37,136,46,202,179,145,223,7,220,246,33,120,23,147,18,63,182,65,80,203,25,119,5,128,16,10,247,71,6,143,127,102,236,94,2,58,60,49,51,74,217,110,100,199,87,227,84,28,53,232,96,73,249,181,174,159,241,91,108,245,233,229,253,226,44,235,117,185,30,124,153,140,155,4,134,8,189,93,154,89,175,139,239,70,123,111,170,59,105,82,168,161,13,184,41,99,198,81,212,210,32,167,244,85,47,197,190,98,158,115,205,79,177,26,183,125,72,45,204,165,196,62,133,240,178,132,151,29,109,76,142,97,68,191,34,40,166,107,12,3,222,17,207,61,1,54,206,162,156,225,219,103,173,27,221,211,52,101,78,113,133,75,52,37,158,3,114,118,237,221,213,7,187,195,225,174,13,19,177,139,239,73,157,130,138,208,47,0,78,169,50,106,218,153,64,35,148,254,186,131,11,244,235,42,240,163,112,184,243,95,9,161,60,236,185,57,59,168,102,135,111,119,205,197,14,96,249,124,56,207,178,41,246,127,31,6,115,105,62,142,232,202,126,166,241,92,179,87,234,110,70,107,143,165,1,88,248,147,58,15,109,108,90,172,201,194,159,93,33,83,65,251,94,176,210,134,121,250,38,97,129,82,217,151,233,17,228,175,22,2,66,211,242,214,63,150,193,116,26,80,43,16,216,209,5,103,245,224,146,85,192,12,36,196,32,227,125,226,113,255,204,128,230,215,145,76,48,25,156,144,155,132,34,136,140,21,72,69,51,74,39,149,67,123,154,46,101,45,23,160,190,29,152,229,30,162,79,120,219,183,28,77,8,55,98,212,170,164,198,61,81,91,53,20,24,247,104,167,68,182,18,223,122,117,84,49,220,181,99,200,4,206,253,203,100,238,71,189,137,252,222,89,44,141,40,191,171,173,10,231,54,180,188,86,27,199,77,199,212,226,160,213,110,148,245,156,125,24,45,231,74,225,31,157,35,206,50,238,149,127,5,164,247,112,130,132,1,150,33,30,53,100,131,141,75,253,55,139,177,204,242,158,102,81,109,159,65,142,83,92,59,246,120,114,239,20,49,222,28,61,178,173,181,185,165,60,11,161,207,254,229,169,25,48,184,101,76,4,179,7,151,52,62,137,26,99,97,108,106,82,14,188,51,121,232,93,241,248,2,57,107,250,63,43,22,191,219,255,9,202,13,237,88,214,84,203,220,201,44,78,233,37,187,124,182,116,224,235,104,210,8,122,19,38,209,186,115,133,68,69,240,190,168,123,205,134,192,56,251,175,119,153,15,72,80,211,54,47,223,86,23,167,90,64,208,85,39,73,155,0,17,230,111,66,195,71,40,113,166,140,87,143,193,227,154,126,216,117,194,3,34,221,89,145,217,138,105,10,243,176,147,170,189,215,79,174,18,129,228,236,70,94,32,136,218,118,144,16,21,197,252,46,196,244,200,135,146,234,29,12,172,98,91,95,183,42,6,41,163,249,27,67,103,128,152,162,36,58,180,171,198,96,247,142,248,245,198,254,40,154,144,216,147,39,160,3,29,170,106,91,61,113,164,141,241,44,57,38,45,33,168,49,53,159,93,72,218,184,177,125,232,47,94,157,121,153,66,204,95,192,110,255,191,171,43,130,107,79,237,167,201,124,108,101,173,150,48,145,228,99,16,22,2,149,9,139,90,183,122,166,235,1,8,97,140,233,115,185,117,222,83,217,118,64,65,52,0,250,230,236,128,123,74,165,169,136,11,249,26,213,200,199,98,175,31,163,88,37,10,102,197,242,138,181,240,161,25,23,105,223,28,180,226,78,132,4,81,129,58,219,21,134,120,112,202,210,158,253,36,103,62,7,67,41,151,86,73,182,5,205,30,77,54,12,174,176,63,32,244,82,189,146,109,55,215,143,20,243,152,137,246,56,203,207,190,35,186,104,96,80,19,92,126,6,59,111,13,227,220,155,71,196,42,100,239,60,18,89,172,84,178,135,46,69,17,231,209,208,224,34,127,116,70,252,238,156,27,195,119,85,234,14,225,76,214,251,211,87,229,188,24,50,193,68,221,179,148,15,114,133,187,162,194,75,51,131,212,206,189,196,178,191,140,180,98,208,218,146,217,109,234,73,87,224,32,17,119,59,238,199,187,102,115,108,103,107,226,123,127,213,23,2,144,242,251,55,162,101,20,215,51,211,8,134,21,138,36,181,245,225,97,200,33,5,167,237,131,54,38,47,231,220,122,219,174,41,90,92,72,223,67,193,16,253,48,236,161,75,66,43,198,163,57,243,63,148,25,147,60,10,11,126,74,176,172,166,202,49,0,239,227,194,65,179,80,159,130,141,40,229,85,233,18,111,64,44,143,184,192,255,186,235,83,93,35,149,86,254,168,4,206,78,27,203,112,145,95,204,50,58,128,152,212,183,110,45,116,77,9,99,221,28,3,252,79,135,84,7,124,70,228,250,117,106,190,24,247,216,39,125,157,197,94,185,210,195,188,114,129,133,244,105,240,34,42,26,89,22,52,76,113,37,71,169,150,209,13,142,96,46,165,118,88,19,230,30,248,205,100,15,91,173,155,154,170,104,53,62,12,182,164,214,81,137,61,31,160,68,171,6,156,177,153,29,175,246,82,120,139,14,151,249,222,69,56,207,241,232,136,1,121,201,158]


    # ret=''
    #小分配 16个字节 内存
    ret=[]
    ret=getlist(ret,16)

    data=transformInt(input)

    # print(Xbox_)
    v9=0
    while(v9<9):
        s = v9
        i=0
        while(i<4):
            v11 = i
            # print(str(data[2+4*v11]))

            # print (str( (data[2 + 4 * v11] * 4 + 2048 + 4096 * v11 + 16384 * s) // 4 + 1))

            # print(Xbox_[(data[2 + 4 * v11] * 4 + 2048 + 4096 * v11 + 16384 * s) / 4 + 1])

            v13 = hextoint(Xbox_[(data[2 + 4 * v11] * 4 + 2048 + 4096 * v11 + 16384 * s) // 4 ])
            v14 = hextoint(Xbox_[(data[3 + 4 * v11] * 4 + 3072 + 4096 * v11 + 16384 * s) // 4 ])
            v15 = hextoint(Xbox_[(data[0 + 4 * v11] * 4 + 4096 * v11 + 16384 * s) // 4 ])
            V17 = hextoint(Xbox_[(data[1 + 4 * v11] * 4 + 1024 + 4096 * v11 + 16384 * s) // 4])

            # data[3 + 4 * v11] = Long_Or(salt[16 * salt[16 * Long_And(v15, 15) + Long_And(V17, 15)+1] + salt[
            #     16 * Long_And(v13, 15) + Long_And(v14, 15)+1]+1], 16 * salt[
            #                                 16 * salt[Long_And(v15, 240) + UBFX(V17, 4, 4)+1] + salt[
            #                                     Long_And(v13, 240) + UBFX(v14, 4, 4)+1]+1])

            data[3 + 4 * v11] = Long_Or(salt[16 * salt[16 * Long_And(v15, 15) + Long_And(V17, 15) ] + salt[16 * Long_And(v13,15) + Long_And(v14, 15) ] ], 16 * salt[16 * salt[Long_And(v15, 240) + UBFX(V17, 4, 4)] + salt[Long_And(v13, 240) + UBFX(v14, 4, 4)]])
            b = salt[16 * salt[Long_And(Long_Sar(v15, 24), 240) + Long_Sar(V17, 28)] + salt[Long_And(Long_Sar(v13, 24), 240) + Long_Sar(v14, 28) ] ]
            c = salt[16 * salt[Long_And(Long_Sar(v15, 20), 240) + Long_And(UBFX(V17, 24, 4), 15) ] + salt[Long_And(Long_Sar(v13, 20), 240) + Long_And(UBFX(v14, 24, 4), 15) ] ]
            d = salt[16 * salt[Long_And(Long_Sar(v15, 16), 240) + Long_And(UBFX(V17, 20, 4), 15) ] + salt[Long_And(Long_Sar(v13, 16), 240) + Long_And(UBFX(v14, 20, 4), 15) ] ]
            e = salt[16 * salt[Long_And(Long_Sar(v15, 12), 240) + Long_And(UBFX(V17, 16, 4), 15) ] + salt[Long_And(Long_Sar(v13, 12), 240) + Long_And(UBFX(v14, 16, 4), 15) ] ]

            data[2 + 4 * v11] = Long_Or(salt[16 * salt[Long_And(Long_Sar(v15, 4), 240) + Long_And(UBFX(V17, 8, 4), 15)] +salt[Long_And(Long_Sar(v13, 4), 240) + Long_And(UBFX(v14, 8, 4), 15)]], 16 * salt[16 * salt[Long_And(Long_Sar(v15, 8), 240) + UBFX(V17, 12, 4)] + salt[Long_And(Long_Sar(v13, 8), 240) + UBFX(v14, 12,
                                                                                                              4) ]])
            data[1 + 4 * v11] = Long_Or(e, 16 * d)
            data[0 + 4 * v11] = Long_Or(c, 16 * b)
            i+=1


        data = transformInt2(data)
        v9+=1

    # print('box=',str(len(BOX)))

    print('data=',data)
    # v22 = 36864
    i = 0
    v22=0
    while(i<16):
        # print(str(data[i] + v22))

        ret[i] = BOX[data[i]+v22+1]
        v22 = v22 + 256
        i = i + 1

    print('ret=',ret)
    return ret

def _Befor_Base64(data):
    # hex string 转 list
    salt_ = list(bytearray.fromhex('A04A65BC134DEFDA38366B34A8326F4C'))
    byte_list=[]
    i=0

    while(i<4):
        byte_list.extend(befor_Base64(data[(i*16):(i*16+16)]))


        # befor_Base64(取文本中间(data,i * 16, 16))
        i+=1
    byte_list.extend(salt_)
    byte_list.extend(salt_)
    print('base_b',byte_list)

    return  SecurityUtil.base64encodebyte(bytes(byte_list))

def getsig3(input):
    str1=long_SHA(input)
    # str1='f301061d2fbc23d3081050abcaceaf71e49a84b01f9b8fff86cb4e44e48e4e78'
    print(str1)
    str2=_Befor_Base64(str1)
    print(str2)

    #正确的
    # str2="YNPO1Zp1JcxgJaJjuNcZTiyTVkcsVef5yOpf2MmS+7l7GopR2k1miNHs2Pwco0MIkZDzw8VkMjqfsMKVpWezeKBKZbwTTe/aODZrNKgyb0ygSmW8E03v2jg2azSoMm9M"

    str3=long_SHA(str2)
    t = TimeUtil.getTime(0)
    r = str(t ^ 0xDDCC0DEF)
    result = r + '9c6'+str3[0:30]

    print(result)
    # return result


def circular_shift_left(int_value, k, bit=8):

    bit_string = '{:0%db}' % bit

    bin_value = bit_string.format(int_value)  # 8 bit binary

    bin_value = bin_value[k:] + bin_value[:k]

    int_value = int(bin_value, 2)

    return int_value


# right circular shift

def circular_shift_right(int_value, k, bit=32):

    bit_string = '{:0%db}' % bit

    #十进制 转2进制
    bin_value = bit_string.format(int_value)  # 8 bit binary
    print(bin_value)
    bin_value = bin_value[-k:] + bin_value[:-k]

    int_value = int(bin_value, 2)
    print(int_value)
    return int_value


DIGITS = {'0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9}

def char2num(s):
    return DIGITS[s]

def str2int(s):
    return reduce(lambda x, y: x * 10 + y, map(char2num, s))

if __name__ == '__main__':
    #1330894883

    # print(ADD(3554434260,2071427919))

    # circular_shift_right(1130259040,18)

    # em.extend(r)
    # print(em)
    # UBFX(3863849899,4,4)
    initsig3('/rest/n/user/profile/v29f37c8a9e150cd89e083b503821fc565')

    # f301061d2fbc23d3081050abcaceaf71e49a84b01f9b8fff86cb4e44e48e4e78
    # f301061d2fbc23d381050abcaceaf71e49a84b01f9b8fff86cb4e44e48e4e78
    # print(str(len('f301061d2fbc23d381050abcaceaf71e49a84b01f9b8fff86cb4e44e48e4e78')))
    # 0b206cd7d4779683da881b2992a375b00b55fbc5d80fc03b51b44a7d2b7b04a5


    # hexData=[96, 211, 206, 213, 154, 117, 37, 204, 96, 37, 162, 99, 184, 215, 25, 78, 44, 147, 86, 71, 44, 85, 231, 249, 200, 234, 95, 216, 201, 146, 251, 185, 123, 26, 138, 81, 218, 77, 102, 136, 209, 236, 216, 252, 28, 163, 67, 8, 145, 144, 243, 195, 197, 100, 50, 58, 159, 176, 194, 149, 165, 103, 179, 120, 160, 74, 101, 188, 19, 77, 239, 218, 56, 54, 107, 52, 168, 50, 111, 76, 160, 74, 101, 188, 19, 77, 239, 218, 56, 54, 107, 52, 168, 50, 111, 76]
    # print(SecurityUtil.base64encodebyte(bytes(hexData)))
    # print(list(SecurityUtil.base64decode('YNPO1Zp1JcxgJaJjuNcZTiyTVkcsVef5yOpf2MmS+7l7GopR2k1miNHs2Pwco0MIkZDzw8VkMjqfsMKVpWezeKBKZbwTTe/aODZrNKgyb0ygSmW8E03v2jg2azSoMm9M')))





