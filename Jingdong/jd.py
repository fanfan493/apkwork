import cv2
import time
import numpy as np
from selenium import webdriver
from urllib import request
from selenium.webdriver.common.action_chains import ActionChains
import random
from selenium.webdriver import ChromeOptions

from pymouse import PyMouse
from pykeyboard import PyKeyboard


option = ChromeOptions()
option.add_experimental_option('excludeSwitches', ['enable-automation'])
brower=""
# brower = webdriver.Chrome("/Users/yangfan/Downloads/chromedriver",options=option)
# brower.maximize_window()


m = PyMouse()
k = PyKeyboard()


def loadpage(userid, password):

    url = "https://passport.jd.com/new/login.aspx?"
    brower.get(url)
    time.sleep(3)
    s1 = r'//div/div[@class="login-tab login-tab-r"]/a'
    userlogin = brower.find_element_by_xpath(s1)
    userlogin.click()
    # time.sleep(5)
    username = brower.find_element_by_id("loginname")
    username.send_keys(userid)
    userpswd = brower.find_element_by_id("nloginpwd")
    userpswd.send_keys(password)
    # time.sleep(5)
    brower.find_element_by_id("loginsubmit").click()
    time.sleep(3)
    while True:
        try:
            getPic()
            time.sleep(2)
        except Exception as e:
            print(e)
            print("登陆成功----")
            break
    time.sleep(5)

def get_tracks(distance):
    print('distance',distance)
    """
    根据偏移量获取移动轨迹
    :param distance:偏移量
    :return:移动轨迹
    """
    # 移动轨迹
    tracks = []
    # 当前位移
    current = 0
    # 减速阈值
    mid = distance * 4 / 5
    # 计算间隔
    t = 0.2
    # 初速度
    v = 0
    while current < distance:
        if current < mid:
            # 加速度为正2
            a = 5
        else:
            # 加速度为负3
            a = -3
        # 初速度v0
        v0 = v
        # 当前速度
        v = v0 + a * t
        # 移动距离
        move = v0 * t + 1 / 2 * a * t * t
        # 当前位移
        current += move
        # 加入轨迹
        tracks.append(round(move))
    return tracks



def getPic():
    # 用于找到登录图片的大图
    s2 = r'//div/div[@class="JDJRV-bigimg"]/img'
    # 用来找到登录图片的小滑块
    s3 = r'//div/div[@class="JDJRV-smallimg"]/img'
    bigimg = brower.find_element_by_xpath(s2).get_attribute("src")
    smallimg = brower.find_element_by_xpath(s3).get_attribute("src")
    # print(smallimg + '\n')
    # print(bigimg)
    # 背景大图命名
    backimg = "backimg.png"
    # 滑块命名
    slideimg = "slideimg.png"
    # 下载背景大图保存到本地
    request.urlretrieve(bigimg, backimg)
    # 下载滑块保存到本地
    request.urlretrieve(smallimg, slideimg)

    # 获取图片并灰度化
    block = cv2.imread(slideimg, 0)
    template = cv2.imread(backimg, 0)
    # # 二值化后的图片名称
    # blockName = "block.jpg"
    # templateName = "template.jpg"
    # # 将二值化后的图片进行保存
    # cv2.imwrite(blockName, block)
    # cv2.imwrite(templateName, template)
    # block = cv2.imread(blockName)
    # block = cv2.cvtColor(block, cv2.COLOR_RGB2GRAY)
    # block = abs(255 - block)
    # cv2.imwrite(blockName, block)
    # block = cv2.imread(blockName)
    # template = cv2.imread(templateName)
    # 获取偏移量
    # result = cv2.matchTemplate(block, template, cv2.TM_CCOEFF_NORMED)  # 查找block在template中的位置，返回result是一个矩阵，是每个点的匹配结果
    # for i in result:
    #     print(i)



    h, w = template.shape[:2]  # rows->h, cols->w
    # 相关系数匹配方法：cv2.TM_CCOEFF
    res = cv2.matchTemplate(block, template, cv2.TM_CCOEFF_NORMED)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

    # left_top = max_loc  # 左上角
    # right_bottom = (left_top[0] + w, left_top[1] + h)  # 右下角
    # cv2.rectangle(block, left_top, right_bottom, 255, 2)  # 画出矩形位置
    print( min_val, max_val, min_loc, max_loc)

    min =  abs(min_val)
    max = abs(max_val)

    if min > max:
        x = min_loc[0]
    else:
        x = max_loc[0]

    print('x',x)
    tracks = get_tracks(int(x)-25)
    print('tracks',tracks)
    # 获取滑块
    element = brower.find_element_by_xpath(s3)
    x=element.location.get("x")
    y=element.location.get("y")+130
    print("x=",x," y=",y)

    # m.move(x,y)
    m.press(x,y)
    for i in tracks:
        print("track i",i)
        m.move(x+i+2,y)

    # m.move(x-3,y)
    # m.move(x+3,y)
    time.sleep(1)
    m.release(x,y)

    # ActionChains(brower).click_and_hold(on_element=element).perform()
    # for i in tracks:
    #     ActionChains(brower).move_by_offset(xoffset=i, yoffset=0).perform()
    #
    # ActionChains(brower).move_by_offset(xoffset=-3, yoffset=0).perform()
    # ActionChains(brower).move_by_offset(xoffset=3, yoffset=0).perform()
    # time.sleep(1)
    # ActionChains(brower).release().perform()

    time.sleep(2)




if __name__ == '__main__':
    id = "18064099492"  # 用户账号
    passwd = "858198110"  # 用户密码
    # loadpage(id, passwd)
    time.sleep(2)
    y=500
    y2=1066
    m.move(920, y)
    time.sleep(2)
    m.press(920,y)
    m.move(y2, y)
    time.sleep(1)
    m.release(y2,y)

