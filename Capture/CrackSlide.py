from urllib.request import urlretrieve
from bs4 import BeautifulSoup
import re
from PIL import Image




class CrackGeetest():

    # 获取图片信息
    def get_image_info(img):
        '''
        :param img: (Str)想要获取的图片类型：带缺口、原始
        :return: 该图片(Image)、位置信息(List)
        '''

        # 将网页源码转化为能被解析的lxml格式
        soup = BeautifulSoup(browser.page_source, 'lxml')
        # 获取验证图片的所有组成片标签
        imgs = soup.find_all('div', {'class': 'gt_cut_' + img + '_slice'})
        # 用正则提取缺口的小图片的url，并替换后缀
        img_url = re.findall('url\(\"(.*)\"\);', imgs[0].get('style'))[0].replace('webp', 'jpg')
        # 使用urlretrieve()方法根据url下载缺口图片对象
        urlretrieve(url=img_url, filename=img + '.jpg')
        # 生成缺口图片对象
        image = Image.open(img + '.jpg')
        # 获取组成他们的小图片的位置信息
        position = get_position(imgs)
        # 返回图片对象及其位置信息

    # 获取小图片位置
    def get_position(img):
        '''
        :param img: (List)存放多个小图片的标签
        :return: (List)每个小图片的位置信息
        '''

        img_position = []
        for small_img in img:
            position = {}
            # 获取每个小图片的横坐标
            position['x'] = int(re.findall('background-position: (.*)px (.*)px;', small_img.get('style'))[0][0])
            # 获取每个小图片的纵坐标
            position['y'] = int(re.findall('background-position: (.*)px (.*)px;', small_img.get('style'))[0][1])
            img_position.append(position)
        return img_position


