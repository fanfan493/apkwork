from Util import ArmUtil
from Util import StringUtil

table="D6283B717076BE1BA4FE19575E6CBC21B214377D8CA2FA67556A95E3FA6778ED8E553389A8CE36B35CD6B26F96C434B96AEC3495C4FA72FFB8428DFBEC70F08546D8B2A1E0CEAE4B7DAEA487CEE3AC5155C436ADFCC4EA97706A85376AC868FAFEB033B9677ECEE3CC86D69F767489E9DA9C78C595AAB034B3F27DB2A2EDE0B5B68895D151D69E7DD1C8F9B770CC9CB692C5FADD9F28DAC7E0CA95B2DA3497CE74FA37E97DC4A237FBFAF1CFAA897D55AE87BCF5E96AC468C7FA768514D0D0E5CEFF19D6E5D6CCF1F46CE9E789B2B7AE2889BE5EDC876CF751F26778AEB34BA2B3213B55F8B376B2CFB3B3FFB35E717DFAFCFFA87DFED89C1BC46AF988B5E5"
print(str(len(table)))
class Gorgon():
    def __init__(self):
        return

    def input(self,time,intputBytes):
        list=[]
        for i in range(4):
            if intputBytes[i]<0:
               list.append(ArmUtil.inttohex(intputBytes[i])[6:])
            else:
               list.append(ArmUtil.inttohex(intputBytes[i]))

        for j in range(4):
            list.append("00")

        for k in range(32,36):
            if intputBytes[k] < 0:
                list.append(ArmUtil.inttohex(intputBytes[k])[6:])
            else:
                list.append(ArmUtil.inttohex(intputBytes[k]))

        for l in range(4):
            list.append("00")

        text=ArmUtil.inttohex(time)

        for m in range(4):
            list.append(text[2*m:2*m+2])

        return list

    def initialize(self,data):
        num=0
        bytetable2=bytearray(bytes.fromhex(table))
        for i in range(len(data)):
            num2=0
            if i==0:
                num2=bytetable2[bytetable2[0]-1]
                bytetable2[i]=num2
            elif i==1:
                num3=ArmUtil.hextoint("D6")+ArmUtil.hextoint("28")
                if num3>256:
                    num3=num3-256
                num2=bytetable2[num3-1]
                num=num3
                bytetable2[i]=num2
            else:
                if i==14:
                    print("111")

                num4=num+bytetable2[i]
                if num4>256:
                    num4=num4-256
                num2=bytetable2[num4-1]
                num=num4
                bytetable2[i]=num2

            if num2*2>256:
                num2=num2*2-256
            else:
                num2=num2*2

            t1=bytetable2[num2-1]
            t2=ArmUtil.hextoint(data[i])
            data[i]=ArmUtil.inttohex(t1 ^ t2)

        return data


    def handle(self,data):
        for i in range(len(data)):
            text=data[i]
            if len(text)<2:
                text=text+"0"
            else:
                s=data[i]

                array=StringUtil.toCharArray(data[i])
                text=array[1] +array[0]

            if i< (len(data)-1):
               text=ArmUtil.inttohex(ArmUtil.hextoint(text) ^ ArmUtil.hextoint(data[i+1]) )
            else:
               text=ArmUtil.inttohex(ArmUtil.hextoint(text) ^ ArmUtil.hextoint(data[0]) )

            t1=int((ArmUtil.hextoint(text) & ArmUtil.hextoint("55")) *2)
            t2=int((ArmUtil.hextoint(text) & ArmUtil.hextoint("AA")) /2)
            num=t1 | t2

            x1=int((num & ArmUtil.hextoint("33")) *4)
            x2=int((num & ArmUtil.hextoint("CC")) /4)
            text2=ArmUtil.inttohex(x1|x2)

            if len(text2) >1:
                array2=StringUtil.toCharArray(text2)
                text2=array2[1] +array2[0]
            else:
                text2=text2+"0"

            num2=ArmUtil.hextoint(text2) ^ ArmUtil.hextoint("FF")
            data[i]=ArmUtil.inttohex(num2 ^ ArmUtil.hextoint("14") )

        return data

    def xGorgen(self,time,intputBytes):
        list=[]
        list.append("03")
        list.append("61")
        list.append("41")
        list.append("10")
        list.append("80")
        list.append("00")
        list2=self.input(time,intputBytes)
        # print(list2[0])
        list2=self.initialize(list2)
        list2=self.handle(list2)

        list.extend(list2)

        # print(''.join(['%02x' % b for b in list]))
        text=""
        for i in range(len(list)):
            text2=list[i]
            if len(text2)>1:
                text=text+text2
            else:
                text=text+"0"
                text = text + text2

        return text