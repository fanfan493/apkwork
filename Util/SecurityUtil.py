import base64
import urllib
from Util import StringUtil
from urllib.parse import urlencode, quote
import hashlib

def base64encodebyte(bytes_url):
    # bytes_url = str.encode("utf-8")
    basestr = base64.b64encode(bytes_url)  # 被编码的参数必须是二进制数据
    # print(basestr)
    return StringUtil.bytestostr(basestr)

def base64encode(str):
    bytes_url = str.encode("utf-8")
    basestr = base64.b64encode(bytes_url)  # 被编码的参数必须是二进制数据
    # print(basestr)
    return StringUtil.bytestostr(basestr)
     # basestr

def base64decode(str):

    basestr = base64.b64decode(str)
    # print(list(basestr))
    return basestr

def urlencodeutls(str):

    return quote(str,safe="")
    # return urlencode(str)

def urldecodeutils(str):
    return  urlencode(str)


def md5(data):
    return hashlib.md5(data.encode(encoding='UTF-8')).hexdigest()

