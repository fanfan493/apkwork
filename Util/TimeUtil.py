import time


 # 获取时间戳
def getTime(index=0):
    t = time.time()
    if index==0:  #10位
        return (int(t))
    elif index==1:  #13位
        return  (int(round(t * 1000)))
