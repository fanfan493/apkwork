
def str_to_hex(s):
    return r"/x " +r'/x'.join([hex(ord(c)).replace('0x', '') for c in s])

def hex_to_str(s):
    return ''.join([chr(i) for i in [int(b, 16) for b in s.split(r'/x')[1:]]])

def str_to_bin(s):
    return ' '.join([bin(ord(c)).replace('0b', '') for c in s])

def bin_to_str(s):
    return ''.join([chr(i) for i in [int(b, 2) for b in s.split(' ')]])

#十六进制转十进制
def hextoint(a):
    if(a==''):
        return 0
    return int(a,16)

# def inttohex(data):
#     lin = ['%02X' % i for i in data]
#     print("".join(lin))

def inttohex2(data):
    lin = ['%02X' % i for i in data]
    print(" ".join(lin))

#十进制转十六进制
def inttohex(a):
    b=hex(a)
    r=hex(a)[2:]
    # if(len(r)<8):
    #     r='0'+r
    # if len(r)>2:
    #    r=r[len(r)-2:]
    return r


#二进制转十进制
def twototen(bin):
    # bin = str(input('输入二进制数：'))
    count = 0
    for i in range(0, len(bin)):
        if bin[i] == str(1):
            sum = 2 ** (len(bin) - i - 1)
            count = count + sum
    # print count
    return count

#获得十进制转二进制的字符串
def b2(str1):
    bi2=str(bin(int(str1))).replace('0b','')
    return bi2

# 2进制低位抹除
def lowbit(r):
    s = b2(r)
    if(len(s)>32):
        #获取32个
        s = s[len(s)-32:len(s)]
        # print(s)
        s=  s[s.index("1"):len(s)]

    result = twototen(s)
    return result

def UBFX(Rn,lsb,width):
    bit_string = '{:0%db}' % 32
    # 十进制 转2进制
    e = bit_string.format(Rn)

    while(len(e)<lsb+width):
        e="0"+e

    e=e[len(e)-width-lsb:len(e)-lsb]
    e = int(e, 2)
    return e

def UMLAL(r0, r1, r2, r3):
    # r0 = (r2 * r3) 的低32位 + r0
    # r1 = (r2 * r3) 的高32位 + r1
    return r0,r1

def UMULL(r0, r1, r2, r3):
    # r0 = (r2 * r3) 的低32位
    # r1 = (r2 * r3) 的高32位
    return

def SMLAL(r0, r1, r2, r3):
    # r0 = (r2 * r3) 的低32位 + r0
    # r1 = (r2 * r3) 的高32位 + r1
    return r0,r1

def SMULL(r0,r1,r2, r3):
    r0 = (r2 * r3) #的低32位
    r1 = (r2 * r3) #的高32位
    return r0,r1


def MLA(r1,r2,r3):
    r0 = r1 * r2 + r3
    return r0

#乘法
def MUL(a,b):
    r=a*b
    # r1=lowbit(r)
    return r

#减法
def SUB(a,b):
    r=a-b
    r1=lowbit(r)
    return r1

#加法
def ADD(a,b):
    r=a+b
    r1=lowbit(r)
    return r1

#位异或
def XOR(a,b):
    return  a^b;

#循环右移
def ROR(int_value, k, bit=32):
    bit_string = '{:0%db}' % bit
    bin_value = bit_string.format(int_value)  # 8 bit binary
    bin_value = bin_value[-k:] + bin_value[:-k]
    int_value = int(bin_value, 2)
    return int_value

#循环左移
def LOR(int_value,k,bit =32):
    bit_string = '{:0%db}' % bit
    bin_value = bit_string.format(int_value) # 8 bit binary
    bin_value = bin_value[k:] + bin_value[:k]
    int_value = int(bin_value,2)
    return int_value

def ORR(a,b):
    return a|b;

#取反
def LongNot(a):
    return ~a;

#位取反
def NOT(a):
    return ~a;

#逻辑左移
def LSL(a,n):
    return a<<n

#逻辑右移
def LSR(a,n):
    return a>>n

#与运算
def AND(a,b):
    return a&b;

def BFI(rd,rn,lsb,width):
    ls = ((0xFFFFFFFF) >> (32 - width))
    rn = (rn & ls) << lsb
    ls = ~(ls << lsb)
    rd = rd & ls
    rd = rd | rn
    return rd

# 位清除指令
def BIC(Rn,operand2):
    return AND(Rn,NOT(operand2))




if __name__ == '__main__':
    print("")

