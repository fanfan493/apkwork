import uuid

def bytestostr(b):
     return str(b, encoding="utf-8")

def strtobytes(str):
    b = bytes(str, encoding='utf-8')
    print(b)
    return b

def toCharArray(str):
    array = []
    for c in str:
        array.append(c)
    return array

def getUUID(index):
    if index==0:
        #根据mac地址
        # bf1dfacf-67d8-11e8-9a23-408d5c985711
         return uuid.uuid1()
    elif index==1:
        #生成md5
        # ddb366f5-d4bc-3a20-ac68-e13c0560058f
         return uuid.uuid3(uuid.NAMESPACE_DNS, 'yuanlin')
    elif index==2:
        #随机
        # 144d622b-e83a-40ea-8ca1-66af8a86261c
         return uuid.uuid4()
    elif index==3:
        #同uuid3  sha1
        # 4a47c18d-037a-5df6-9e12-20b643c334d3
         return uuid.uuid5(uuid.NAMESPACE_DNS, 'yuanlin')



